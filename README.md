# micro-photogrammetry

## Course Proposal

### Overview
In the wake of COVID-19, CBA has spun up a number of quick response projects to provide effective PPE and medical supplies using our tools at MIT and leveraging the larger fab community. Fortunate to still have lab access, I have focused on characterizing and testing filter material with various microscopy techniques and am building an Filtration Efficiency Testing Appartus ([FETA](https://gitlab.cba.mit.edu/camblackburn/filter_testing)) to measure the pressure drop and filter efficiency of MERV graded materials and common household items. I have been agregating my results into a database documented [here](https://camblackburn.pages.cba.mit.edu/filter_media/). Throughout this process, I'm curious how we can use microscopic imaging to potentially predict good vs bad filter materials and aid in design of new material. 

We have been collaborating with Dassault Systemes Simulia, using their lattice-boltzmann physics simulation to iterate on PPE designs for optimal fluid containment. They also have a software called  [DigitalROCK](https://www.3ds.com/products-services/simulia/products/digitalrock/) which calculates the relative permeability of resivior rock through a pore-scale two-phase flow simulation. This could be used to simulate the fluid flow through filter material and return the same type of information that the FETA tests can verify. However, the DigitalROCK simulation takes a  3D model as input, and we don't have a process to scan filter material in 3D. I'd like to develop and implement a micro-photogrammetry workflow with the SEM. This technique will require taking many SEM images of a sample material from varying angles to stitch them back together into a 3D model, which can then be fed into the digitalROCK simulation to predict airflow through the material. Assuming success with digitalROCK, the sumlation results could then be compared against experimental measurements from the same sample material in the FETA. 

### Game Plan
I have a bit of previous work experience in computer vision optical flow techniques for tracking posture and motion in CCTV footage and have spent the first half of my year at CBA honing my skills in microscopic imaging with the SEM. This is an interesting opportunity to combine both topics! 

3D model reconstruction with SEM images is something that has been explored before.
- [Three-dimensional reconstruction of highly complex microscopic samples using scanning electron microscopy and optical flow estimation](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5383242/). Ahmadreza Baghaie, Ahmad Pahlavan Tafti, Heather A. Owen, et. al. 
- [3D reconstruction of SEM images by use of optical photogrammetry software](https://www.ncbi.nlm.nih.gov/pubmed/26073969). Eulitz M, Reiss G 
- [SEM-microphotogrammetry, a new take on an old method for generating high-resolution 3D models from SEM images](https://www.ncbi.nlm.nih.gov/pubmed/28328041). Ball AD, Job PA, Walker AEL

and there are some commercial software packages which have been devoped for this use case
- [Mountains from Digital Surf](https://www.digitalsurf.com/video-tutorials/3d-reconstruction-from-a-stereo-pair-of-sem-images/) 
- [Capturing Reality micro-photogrammetry VR/AR experience](https://www.youtube.com/watch?v=fsd7ojhuzec)

Using these projects as a starting point, I will implement a 3D reconstruction algorithm with specific modifications for SEM image disortion. At the onset, I think I'll be using openCV [3D reconstruction](https://docs.opencv.org/2.4/modules/calib3d/doc/camera_calibration_and_3d_reconstruction.html) and [optical flow](https://docs.opencv.org/3.4/d4/dee/tutorial_optical_flow.html) libraries along with some packages from [Point Clound Library](https://docs.opencv.org/3.4/d4/dee/tutorial_optical_flow.html) for stitching together the 3d model. However, I suspect that this toolchian will expand or change as I dive deeper into background research. 


I pressume that I will hit the following speed bumps and aim to answer these questions from them
- how should the samples be mounted to allow for imaging in all directions without needing to open the chamber and reposition it? If repositioning does need to happen, what type of markers can be used to realign the new mount to the old one? 
- fitler materials have many overlapping tangles and knots of the fibers - will this complicate the reconstruction? 
- what adjustments will need to be made for reconstructing with SEM images, as opposed to regular optical images? Will these adjustments be a result of the microscopic size (i.e. they would be the same for optical microscopes) or specific to the SEM? 
- taking a high resolution SEM image can take up to a couple minutes due to the slow scanning speed - so what is the lowest scanning speed and least amount of images that I can take while maintaining a high resolution final 3D model. Since the photogrammetry technique requires overlapping multiple images, could I have the individual images at a lower resolution and average out the noise? I'd like to optimize the data collection with respect to speed as much as possible.


### Evaluation
Baseline success of this project will be determined by the existence of a 3D model of filter material reconstructed by 2D SEM images. The reconstruction algorithm can be compared against another open source photogrammetry pipeline like [AliceVision](https://alicevision.org/) - the comparison can't exactly judge accuracy because AliceVision is not optimized for SEM images, but will hopefully provide insightful evaluation nonetheless.

Then, assuming a model is accomplished, it can be fed into 3DS Simulia DigitalROCK simulation to determine porosity and airlfow. These results will be tested experimentally with the FETA. Since the FETA is also not verified as ground truth, I will use a N95 mask material for the project so that we have government regulated testing to provide a standard for our results.

The imaging methodolody, reconstruction algorithm, and filter efficiency results will be presented in a final report. 
